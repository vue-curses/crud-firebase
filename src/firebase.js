import firebase from 'firebase/app';
import 'firebase/firestore';


const firebaseConfig = {
    apiKey: "AIzaSyBV1fHCEwoyQYmHH8Z1-i-oyb7LGc8XMeI",
    authDomain: "crud-real-c72f8.firebaseapp.com",
    projectId: "crud-real-c72f8",
    storageBucket: "crud-real-c72f8.appspot.com",
    messagingSenderId: "350332310252",
    appId: "1:350332310252:web:dbd00baa3f6eb549bb8bc9"
};

firebase.initializeApp(firebaseConfig);


const db = firebase.firestore();

export {
    db
};