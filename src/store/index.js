import Vue from 'vue'
import Vuex from 'vuex'
import {db} from '../firebase';
import router from '../router';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tareas: [],
    tarea: {nombre: '', id: ''}
  },
  mutations: {
    setTareas(state,payload){
      state.tareas = payload;
    },
    setTarea(state, payload){
      state.tarea = payload;
    },
    setEliminarTarea(state, payload){
      state.tareas = state.tareas.filter(tarea => tarea.id !== payload)
    }
  },
  actions: {
    getTareas({commit}){
      const tareas = [];

        db.collection('tareas').get()
        .then(res=>{
          res.forEach(doc =>{
            let tarea = doc.data();
            tarea.id = doc.id;
            tareas.push(tarea);
          });
          commit('setTareas',tareas);
        });
    },
    getTarea({commit}, idTarea){
      db.collection('tareas').doc(idTarea).get()
      .then(doc =>{
        let tarea = doc.data();
        tarea.id = doc.id;
        commit('setTarea', tarea);
      });
    },
    editarTarea({commit}, tarea){
      db.collection('tareas').doc(tarea.id).update({
        nombre: tarea.nombre
      })
      .then(()=>{
        router.push('/');
      })
    },
    newTarea({commit}, nombre){
        db.collection('tareas').add({
          nombre
        })
        .then(()=>{
          router.push('/');
        })
    },
    eliminarTarea({commit, dispatch}, id){
      db.collection('tareas').doc(id).delete()
      .then(()=>{
        // dispatch('getTareas') //hacer peticiones llamando funciones dentro de action
        commit('setEliminarTarea',id);
      })
    }
  },
  modules: {
  }
})
